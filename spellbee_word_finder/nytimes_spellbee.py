#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import TimeoutException
import subprocess

print("Fetching words...")
options = webdriver.ChromeOptions()
options.add_argument('headless')
driver = webdriver.Chrome(options=options)
driver.set_page_load_timeout(3)
try:
    driver.get("https://www.nytimes.com/puzzles/spelling-bee")
except TimeoutException:
    driver.execute_script("window.stop();")
play_button = driver.find_element_by_class_name("pz-modal__button")

# Click on initial play button on pop-up to start play
action = ActionChains(driver)
action.click(play_button)
action.perform()

# Fetch central element(in yellow)
central_letter_element = driver.find_element_by_class_name("hive-cell.center")
central_letter = central_letter_element.text.lower()
# Fetch outer elements
outer_letter_elements = driver.find_elements_by_class_name("hive-cell.outer")
outer_letters = "" 

for element in outer_letter_elements:
    outer_letters += element.text

outer_letters = outer_letters.lower()
print("The outer elements are {}, the central element is {}".format(
    outer_letters,central_letter))

print("Finding qualifying words...")
# Running bash script to fetch words
get_words = subprocess.Popen(["bash","spellbee.sh",outer_letters,central_letter],
                 stdout = subprocess.PIPE,stderr=subprocess.STDOUT)
stdout,stderr =  get_words.communicate()
stdout = stdout.decode("utf-8")


if(stderr == None):
    print("Suitable words are:")
    for word in stdout.split('\n'):
        word = word.strip()
        print(word)
else:
    print("Oops!! Something went wrong !! Please try again")
                                                                             

