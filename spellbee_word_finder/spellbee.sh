#!/usr/bin/env bash
grep "^[$1$2]*$2[$1$2]*$" /usr/share/dict/words \
	| sed -r "/^.{,3}$/d" \
	| awk '{ if(length == 4)
		    print 1, $0 
		 else 
		    print length, $0 }' \
	| sort -nr 

