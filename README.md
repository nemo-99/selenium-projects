# Selenium Mini-Projects

+ This repository consists of mini-projects implemented by taking advantage of Selenium, which is a tool for automating browsers

## Installation Instructions

+ All projects in general will require the selenium module implemented for python. It can be installed as follows
	
	`pip3 install selenium`

+ In addition to this, the projects also require a webdriver utility for either Firefox or Chrome(I have used Chrome for my projects), installation links for both are shared below

    [Mozilla webdriver(geckodriver) github release page]( https://github.com/mozilla/geckodriver/releases) <br>
    [Chrome Driver Downloads page]( http://chromedriver.chromium.org/downloads)
+ Download either of the files,extract them using tar, grant them executable permissions and move them to `/usr/local/bin` ( this might require super-user privileges)

+ In addition to that, each mini-project can have additional requirements which will be mentioned in a `requirements.txt` file in the respective folder. To install those requirements

    `pip3 install -r requirements.txt`

+ The Description of each project is now given below


## Nytimes Spellbee solver

+ This script recommends you solutions for the most recent [Nytimes Spellbee game]( https://www.nytimes.com/puzzles/spelling-bee) 

+ To execute run

` ./nytimes_spellbee.py`

+ The script will print the possible words in descending order of their character length to the console
